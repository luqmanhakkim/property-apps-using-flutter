import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class Jade extends StatefulWidget {

  @override
  _JadeState createState() => _JadeState();
}

class _JadeState extends State<Jade> {

  //Declaration made for tapping the favorite button activity
  bool _isFavorited = true;

  void _toggleFavorite() {
    setState(() {
      if(_isFavorited) {
        _isFavorited = false;
      } else {
        _isFavorited = true;
      }
    });
  }

  //Widget made to show the picture of the property along with the property company logo
  //The description of the property was also been described at the bottom of the property picture
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    Widget jadeSection = Container(
        padding: EdgeInsets.all(3),
        child: Column(
          children: <Widget>[
            Row(
              children: <Widget>[
                Image.network(
                    'https://my1-cdn.pgimgs.com/listing/27550991/UPHO.118589984.V800/Jade-Tower-Ampang-Ampang-Malaysia.jpg',
                    width: 200,
                    height: 214,
                    fit: BoxFit.cover),
                Column(
                  children: <Widget>[
                    Image.network(
                        'http://koolproperty.com/wp-content/uploads/2016/07/IMG-20160320-WA0020.jpg',
                        width: 200,
                        height: 110,
                        fit: BoxFit.cover),
                    Image.network(
                        'https://s3media.freemalaysiatoday.com/wp-content/uploads/2019/02/sp-setia-270219.jpg',
                        width: 200,
                        height: 110,
                        fit: BoxFit.cover),
                  ],
                ),
              ],
              mainAxisAlignment: MainAxisAlignment.start,
            ),
            ListTile(
              title: Text('Jade Tower',
                  style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold)),
              subtitle: Text('Ampang'),
              trailing: IconButton(
                  icon: (_isFavorited ? Icon(Icons.favorite_border) : Icon(Icons.favorite)),
                  onPressed: _toggleFavorite),
            ),
            Padding(padding: EdgeInsets.only(bottom: 0.5)),
            Divider(),
          ],
        ));

    return jadeSection;
  }
}
