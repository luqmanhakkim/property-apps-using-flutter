import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class Ceria extends StatefulWidget {

  @override
  _CeriaState createState() => _CeriaState();
}

class _CeriaState extends State<Ceria> {

  //Declaration made for tapping the favorite button activity
  bool _isFavorited = true;

  void _toggleFavorite() {
    setState(() {
      if(_isFavorited) {
        _isFavorited = false;
      } else {
        _isFavorited = true;
      }
    });
  }

  //Widget made to show the picture of the property along with the property company logo
  //The description of the property was also been described at the bottom of the property picture
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    Widget ceriaSection = Container(
      padding: const EdgeInsets.all(3),
      child: Column(
        children: <Widget>[
          Row(
            children: <Widget>[
              Image.network(
                  'http://rescom.my/or/images/listing_photos/18024_dsc0259.jpg',
                  width: 200,
                  height: 214,
                  fit: BoxFit.cover),
              Column(
                children: <Widget>[
                  Image.network(
                    'https://img.rnudah.com/images/48/487921026154030.jpg',
                    width: 200,
                    height: 110,
                    fit: BoxFit.cover,
                  ),
                  Image.network('https://www.umland.com.my/wp-content/uploads/2018/05/umland-color-logo.png',
                  width: 200,
                  height: 110,)
                ],
                mainAxisAlignment: MainAxisAlignment.start,
              ),
            ],
          ),
          ListTile(
            title: Text('Ceria Condominium',
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20)),
            subtitle: Text('Cyberjaya'),
            trailing: IconButton(
              icon: (_isFavorited ? Icon(Icons.favorite_border) : Icon(Icons.favorite)),
              onPressed: _toggleFavorite,
            ),
          ),
          Padding(padding: EdgeInsets.only(bottom: 0.5)),
          Divider(),
        ],
      ),
    );
    return ceriaSection;
  }
}
