import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class Chat extends StatefulWidget {
  @override
  _ChatState createState() => _ChatState();
}

class _ChatState extends State<Chat> {

  //The UI creation at the AppBar title
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        title: Text('Chat Message'),
        backgroundColor: Colors.red,
        actions: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.end,
            crossAxisAlignment: CrossAxisAlignment.end,
            children: <Widget>[
              IconButton(
                icon: Icon(Icons.call, color: Colors.white),
                onPressed: () {},
              ),
              IconButton(
                icon: Icon(Icons.settings, color: Colors.white),
                onPressed: () {},
              ),
            ],
          ),
        ],
      ),
      body: ChatScreen(),
    );
  }
}

class ChatScreen extends StatefulWidget {
  @override
  State createState() => ChatScreenState();
}

//Declaration made for the chat sending section
class ChatScreenState extends State<ChatScreen> {
  final TextEditingController _chatController = new TextEditingController();
  final List<ChatMessage> _messages = <ChatMessage>[];

  void _handleSubmit(String text) {
    _chatController.clear();
    ChatMessage message = new ChatMessage(text: text);

    setState(() {
      _messages.insert(0, message);
    });
  }

  //define particular chat layout where user will type and send message
  //Widget for chat typing section that include smile emoticon and share section
  Widget _chatEnvironment() {
    return IconTheme(
      data: new IconThemeData(color: Colors.blue),
      child: Container(
        margin: const EdgeInsets.symmetric(horizontal: 8.0),
        child: new Row(
          children: <Widget>[
            IconButton(icon: Icon(Icons.add_circle),onPressed: () {}),
            Flexible(
              child: new TextField(
                decoration: new InputDecoration.collapsed(
                    hintText: "Start Typing....."),
                controller: _chatController,
                onSubmitted: _handleSubmit,
              ),
            ),
            IconButton(icon: Icon(Icons.insert_emoticon),onPressed: () {}),
            Container(
              margin: const EdgeInsets.symmetric(horizontal: 4.0),
              child: IconButton(
                icon: Icon(Icons.send),
                onPressed: () => _handleSubmit(_chatController.text),
              ),
            ),
          ],
        ),
      ),
    );
  }

  //Widget build that shows the message input by the user in a form of list
  @override
  Widget build(BuildContext context) {
    return new Column(
      children: <Widget>[
        Flexible(
          child: ListView.builder(
            padding: EdgeInsets.all(8.0),
            reverse: true,
            itemBuilder: (_, int index) => _messages[index],
            itemCount: _messages.length,
          ),
        ),
        new Divider(
          height: 1.0,
        ),
        new Container(
          decoration: BoxDecoration(color: Theme.of(context).cardColor),
          child: _chatEnvironment(),
        ),
      ],
    );
  }
}

String _name = 'Saitama';

class ChatMessage extends StatelessWidget {
  final String text;

  //Constructor to get text from textfield
  ChatMessage({this.text});

  //Widget build that shows the design of the message sent by the user
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return new Container(
      margin: const EdgeInsets.symmetric(vertical: 10.0),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Container(
            margin: EdgeInsets.only(right: 16.0),
            child: CircleAvatar(
              child: Image.network(
                  'https://i.pinimg.com/originals/3b/a4/1e/3ba41ec9dad7c14601a4bd80bbb09940.jpg'),
            ),
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text(_name, style: Theme.of(context).textTheme.subhead),
              new Container(
                margin: const EdgeInsets.only(top: 5.0),
                child: new Text(text),
              )
            ],
          )
        ],
      ),
    );
  }
}
