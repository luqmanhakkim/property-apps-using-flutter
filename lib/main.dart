import 'package:flutter/material.dart';
import 'package:property/ceria.dart';
import 'package:property/chat.dart';
import 'package:property/jade.dart';

void main() => runApp(MyApp());

class MyApp extends StatefulWidget {
  // This widget is the root of your application.

  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  //Declaration made for tapping the favorite button activity
  bool _isFavorited = true;
  var selection = '';
  var title = '';

  void _toggleFavorite() {
    setState(() {
      if (_isFavorited) {
        _isFavorited = false;
      } else {
        _isFavorited = true;
      }
    });
  }

  //This is the widget made to enable the popup button at the top right of the appbar
  @override
  Widget build(BuildContext context) {
    Widget popupMenu = PopupMenuButton(
      onSelected: (String value) {
        setState(() {
          selection = value;
        });
      },
      child: Icon(Icons.share, color: Colors.white),
      itemBuilder: (BuildContext context) => <PopupMenuEntry<String>>[
            PopupMenuItem<String>(
                value: 'Value 1',
                child: Text('Select Conversation',
                    textAlign: TextAlign.center,
                    style:
                        TextStyle(fontSize: 18, fontWeight: FontWeight.bold))),
            PopupMenuItem<String>(
              value: 'Value 1',
              child: ListTile(
                leading: Image.network(
                    'https://tshop.r10s.com/56f/7ac/10fc/1770/5076/0a65/eb33/11b3e98312c45444889cef.jpg'),
                title: Text('Saitama',
                    style:
                        TextStyle(fontWeight: FontWeight.bold, fontSize: 14)),
                subtitle: Text('One Punch Man'),
                trailing: IconButton(
                    icon: Icon(Icons.message),
                    onPressed: () {
                      Navigator.push(context,
                          MaterialPageRoute(builder: (context) => Chat()));
                    }),
              ),
            ),
            PopupMenuItem<String>(
              value: 'Value 2',
              child: ListTile(
                leading: Image.network(
                    'https://cdn.myanimelist.net/images/characters/9/297329.jpg'),
                title: Text('Genos',
                    style:
                        TextStyle(fontWeight: FontWeight.bold, fontSize: 14)),
                subtitle: Text('Cyborg'),
                trailing: IconButton(
                    icon: Icon(Icons.message),
                    onPressed: () {
                      Navigator.push(context,
                          MaterialPageRoute(builder: (context) => Chat()));
                    }),
              ),
            ),
            PopupMenuItem<String>(
              value: 'Value 3',
              child: Text(
                  'Once you start a new conversation, you see it listed here',
                  textAlign: TextAlign.center,
                  style: TextStyle(fontSize: 14)),
            ),
          ],
    );

    //Widget made to show the picture of the property along with the property company logo
    //The description of the property was also been described at the bottom of the property picture
    Widget tiaraSection = Container(
      padding: const EdgeInsets.all(3),
      child: Column(
        children: <Widget>[
          Row(
            children: <Widget>[
              Image.network(
                  'https://my1-cdn.pgimgs.com/listing/25675886/UPHO.106210550.V800/tiara-intan-condo-bukit-indah-bandar-baru-ampang-kosas-Ampang-Malaysia.jpg',
                  width: 200,
                  height: 214,
                  fit: BoxFit.cover),
              Column(
                children: <Widget>[
                  Image.network(
                      'https://media.karousell.com/media/photos/products/2017/08/21/tiara_intan_apartment_for_sale_ampang_1503308132_009f06ed.jpg',
                      width: 200,
                      height: 110,
                      fit: BoxFit.cover),
                  Image.network(
                      'https://assets.nst.com.my/images/articles/22spsetiaST_field_image_socialmedia.var_1498146177.jpg',
                      width: 200,
                      height: 110,
                      fit: BoxFit.cover),
                ],
                mainAxisAlignment: MainAxisAlignment.start,
              ),
            ],
          ),
          ListTile(
            title: Text('Tiara Intan Condo',
                style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold)),
            subtitle: Text('Bukit Indah Ampang'),
            trailing: IconButton(
              icon: (_isFavorited
                  ? Icon(Icons.favorite_border)
                  : Icon(Icons.favorite)),
              onPressed: _toggleFavorite,
            ),
          ),
          Padding(padding: EdgeInsets.only(bottom: 0.5)),
          Divider()
        ],
      ),
    );
    return MaterialApp(
      title: 'Property Hunter',
      home: Scaffold(
        drawer: Drawer(
          child: new ListView(
            padding: EdgeInsets.zero,
            children: <Widget>[
              DrawerHeader(
                child: Container(
                    padding: EdgeInsets.all(3),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text('Profile',
                            textAlign: TextAlign.center,
                            style: TextStyle(
                                fontWeight: FontWeight.bold,
                                fontSize: 18,
                                color: Colors.yellow,
                                letterSpacing: 2)),
                        Padding(
                          padding: EdgeInsets.all(3.0),
                        ),
                        Row(
                          children: <Widget>[
                            Image.network(
                              'https://cdn.myanimelist.net/images/characters/16/285584.jpg',
                              width: 100,
                              height: 100,
                              fit: BoxFit.cover,
                            ),
                            Column(
                              children: <Widget>[
                                Text('Fubuki',
                                    style: TextStyle(
                                        fontWeight: FontWeight.bold,
                                        fontSize: 16,
                                        color: Colors.white)),
                                Text('Class B',
                                    style: TextStyle(color: Colors.white)),
                              ],
                            ),
                          ],
                        )
                      ],
                    )),
                decoration: BoxDecoration(
                  color: Colors.blue,
                ),
              ),
              ListTile(
                leading: Icon(Icons.photo),
                title: Text('Photos'),
              ),
              ListTile(
                leading: Icon(Icons.notifications),
                title: Text('Notifications'),
              ),
              ListTile(
                leading: Icon(Icons.settings),
                title: Text('Settings'),
              ),
              Divider(
                color: Colors.black,
                indent: 16.0,
              ),
              ListTile(
                title: Text('About Us'),
              ),
              ListTile(
                title: Text('Privacy'),
              ),
            ],
          ),
        ),
        appBar: AppBar(
          title: Text('Property Hunter'),
          backgroundColor: Colors.red,
          actions: <Widget>[
            Builder(builder: (context) => popupMenu),
          ],
        ),
        body: ListView(
          shrinkWrap: true,
          children: <Widget>[
            tiaraSection,
            Ceria(),
            Jade(),
          ],
        ),
      ),
    );
  }
}
